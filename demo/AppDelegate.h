//
//  AppDelegate.h
//  demo
//
//  Created by Çağatay Özcan on 2.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
    MainViewController *mainViewController;
    UINavigationController *navCont;
    UIWindow *window;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) UINavigationController *navCont;
@property (nonatomic, retain) MainViewController *mainViewController;

@end


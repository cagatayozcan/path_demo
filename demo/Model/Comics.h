//
//  Comics.h
//  demo
//
//  Created by Çağatay Özcan on 7.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comics : NSObject

@property(nonatomic, strong) NSNumber *comicsId;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *thumbnailUrl;

@end


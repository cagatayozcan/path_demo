//
//  Page.m
//  demo
//
//  Created by Çağatay Özcan on 2.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import "Page.h"

@implementation Page

- (instancetype)initWithLimit:(NSUInteger)limit andOffset:(NSUInteger)offset {
    self = [self init];
    
    if (self) {
        self.limit = limit;
        self.offset = offset;
    }
    
    return self;
}

@end

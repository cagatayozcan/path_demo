//
//  Page.h
//  demo
//
//  Created by Çağatay Özcan on 2.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Page : NSObject

@property(nonatomic, assign) NSUInteger limit;
@property(nonatomic, assign) NSUInteger offset;

- (instancetype)initWithLimit:(NSUInteger)limit andOffset:(NSUInteger)offset;

@end

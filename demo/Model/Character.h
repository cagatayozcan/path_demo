//
//  Character.h
//  demo
//
//  Created by Çağatay Özcan on 2.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Character : NSObject

@property(nonatomic, strong) NSNumber *characterId;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *thumbnailUrl;
@property(nonatomic, strong) NSString *characterDescription;

@end

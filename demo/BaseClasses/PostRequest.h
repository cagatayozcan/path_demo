//
//  PostRequest.h
//  demo
//
//  Created by Kaplan on 2.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define	MAXDATADOWNLOAD					10000000			// NSURLConnection will stop downloading after this much bytes

@interface PostRequest : NSObject {
    
	BOOL						fDumpData;				// for debugging, dumps downloaded response to the log.
	
	NSURLConnection				*urlConnection;
	NSMutableData				*responseData;
	BOOL						fLoadFailed;
	
	SEL							requestFinishedSelector;
	id							requestFinishedTarget;
	
	NSString					*functionName;
	NSString					*moduleName;
	NSString					*baseURL;
	NSString					*httpParameters;
	NSString					*xmlParameters;
	NSString					*base64Parameters;
    NSString                    *token;
}

@property (strong, nonatomic) NSString *token;

- (id) initWithBaseURL:(NSString *)baseURL;
- (void) addRequestFinishedSelector:(SEL)doneSelector target:(id)target;
- (void) start;

+(PostRequest *) requestFromModule:(NSString *)moduleName
httpParameters:(NSDictionary *)httpParams
baseURL:(NSString *)baseURL
token:(NSString *)token;

+(PostRequest *) requestFromModule:(NSString *)moduleName
function:(NSString *)functionName
base64Parameters:(NSString *)base64Params
baseURL:(NSString *)baseURL;

+(NSString *) httpParamsFromDictionary:(NSDictionary *) d;
+(NSString *) base64ParamsFromString:(NSString *) s;
+(NSDictionary *) dictionaryForLoginUser:(NSString *)uname password:(NSString *)pass;

@property (nonatomic, strong) NSString *baseURL;
@property (nonatomic, strong) NSString *functionName;
@property (nonatomic, strong) NSString *moduleName;
@property (nonatomic, strong) NSString *httpParameters;
@property (nonatomic, strong) NSString *xmlParameters;
@property (nonatomic, strong) NSString *base64Parameters;
@property (nonatomic, strong) id context;
@property (nonatomic, strong) NSMutableData	*responseData;
@property (nonatomic, assign) BOOL fLoadFailed;
@property (nonatomic, assign) BOOL fDumpData;

@end

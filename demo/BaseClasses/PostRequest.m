//
//  PostRequest.m
//  demo
//
//  Created by Kaplan on 2.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import "PostRequest.h"

@implementation PostRequest

@synthesize moduleName;
@synthesize functionName;
@synthesize baseURL;
@synthesize httpParameters;
@synthesize xmlParameters;
@synthesize base64Parameters;
@synthesize responseData;
@synthesize fLoadFailed;
@synthesize fDumpData;
@synthesize context;
@synthesize token;

+(PostRequest *) requestFromModule:(NSString *)moduleName httpParameters:(NSDictionary *) httpParams baseURL:(NSString *)baseURL token:(NSString *)token	{
	
	PostRequest *newRequest;
	newRequest = [[PostRequest alloc] initWithBaseURL:baseURL];
	newRequest.moduleName = moduleName;
    newRequest.token = token;
	if ( [httpParams count] < 1 )
		newRequest.httpParameters = @"";
	else
		newRequest.httpParameters = [PostRequest httpParamsFromDictionary:httpParams];
	[newRequest start];
	return newRequest;
}


+(PostRequest *) requestFromModule:(NSString *)moduleName function:(NSString *)functionName base64Parameters:(NSString *) base64Params baseURL:(NSString *)baseURL	{
	
	PostRequest *newRequest;
	
	newRequest = [[PostRequest alloc] initWithBaseURL:baseURL];
	newRequest.functionName	= functionName;
	newRequest.moduleName = moduleName;
	newRequest.base64Parameters = [PostRequest base64ParamsFromString:base64Params];
	[newRequest start];
	return newRequest;
}


-(id) initWithBaseURL:(NSString *)bu	{
	
	if ( self = [super init] )	{
		urlConnection = nil;
		responseData = nil;
		functionName = nil;
		moduleName = nil;
		httpParameters = nil;
		base64Parameters = nil;
		fLoadFailed = NO;
		self.baseURL = bu;
		fDumpData = NO;
	}
	
	return self;
}

-(void)start	{
    
	NSString *urlString;
	if ( httpParameters )	{
        urlString = [NSString stringWithFormat:@"%@/%@", baseURL, moduleName];
	}
    
	else if ( base64Parameters )	{
		urlString = [NSString stringWithFormat:@"%@/%@?%@", baseURL, moduleName, base64Parameters];
	}
	else {
		NSLog( @"httpParameters and xmlParameters should not both be nil!", nil );
		return;
	}
    
    
	NSURL *url = [NSURL URLWithString:urlString];

	NSMutableURLRequest *urlReq = [NSMutableURLRequest requestWithURL:url];
    
    if (httpParameters){
                
        NSData *data2 = [httpParameters dataUsingEncoding:NSASCIIStringEncoding];
        NSString *nslength = [[NSString alloc] initWithFormat:@"%d",[data2 length]];
        [urlReq setHTTPMethod:@"POST"];
        [urlReq setValue:nslength forHTTPHeaderField:@"Content-Length"];
        [urlReq setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        
        if(![token isEqualToString:@""]){
            [urlReq setValue:token forHTTPHeaderField:@"x-access-token"];
        }
        
        [urlReq setHTTPBody:data2];
    }
    
	urlConnection = [NSURLConnection connectionWithRequest:urlReq delegate:self];
	responseData = [[NSMutableData alloc] init];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
}

-(void) addRequestFinishedSelector:(SEL)doneSelector target:(id)target	{
    
	requestFinishedTarget = target;
	
	requestFinishedSelector = doneSelector;
}

/*-(void) dealloc	{
	NSLog( @"PostSpot Request Deallocating.", nil );
	
}*/

+(NSString *) httpParamsFromDictionary:(NSDictionary *) d	{
    
    
	NSString *httpParams = @"";
	NSArray *allKeys = [d allKeys];
	int paramCount = 0;
	for ( NSString *key in allKeys )	{
		NSString *value = [d valueForKey:key];
        

        
        httpParams = [httpParams stringByAppendingFormat:@"%@=%@", key,value];
        
		paramCount++;
		if ( paramCount < [allKeys count] )
			httpParams = [httpParams stringByAppendingString:@"&"];
	}
    
    
    
    //return httpParams;
    
	return [httpParams stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

+(NSString *) base64ParamsFromString:(NSString *) s	{
	
	return [NSString stringWithFormat:@"PAR=%@",s];
}


#pragma mark HTTP Delegate

- (void)connectionDidFinishLoading:(NSURLConnection *)connection	{
	//NSLog( @"Finished downloading [%@].", moduleName );
	if ( fDumpData )	{
		NSString *txt = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
		//NSLog( @"Data: [%@].", txt );
	}
	
	fLoadFailed = NO;
	if ( [requestFinishedTarget respondsToSelector:requestFinishedSelector] )	{
		[requestFinishedTarget performSelector:requestFinishedSelector withObject:self];
	}
	else {
		NSLog( @"ConnectionDidFinishLoading: requestFinishedTarget is not valid.", nil );
	}
    
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data	{
	// TODO: this size checking should be done oin didReceiveResponse method
	if ( [responseData length] < MAXDATADOWNLOAD )	{
		[responseData appendData:data];
	}
	else	{
		[connection cancel];
		[self connection:urlConnection didFailWithError:nil];
		NSLog( @"D_MAXDATADOWNLOAD exceeded. Connection aborted [%@].", functionName );
	}
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error	{
	self.responseData = nil;
	fLoadFailed = YES;
	[requestFinishedTarget performSelector:requestFinishedSelector withObject:self];
	NSLog( @"Err:%@", error );
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSHTTPURLResponse
																	 *)response
{
	// this method is called when the server has determined that it
    // has enough information to create the NSURLResponse
    // it can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    // receivedData is declared as a method instance elsewhere
    [responseData setLength:0];
	//NSLog( @"Cookie from cookie storge: [%@]", [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies] );
}

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    //	if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    //		if ([trustedHosts containsObject:challenge.protectionSpace.host])
    [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}



@end

//
//  NSString+MD5.h
//  demo
//
//  Created by Çağatay Özcan on 2.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString(MD5)

- (NSString *)md5;

@end

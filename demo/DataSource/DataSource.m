//
//  DataSource.m
//  demo
//
//  Created by Çağatay Özcan on 2.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import "DataSource.h"
#import "NSString+MD5.h"
#import <AFNetworking/AFNetworking.h>
#import <CommonCrypto/CommonDigest.h>
#import "Page.h"
#import "Character.h"
#import "Comics.h"

#define PUBLIC_API_KEY      @"0a1fd94daea90db3a6d700e7d6bb6f0d"
#define PRIVATE_API_KEY     @"9f27eccfafba680d62b9c054787a48e65107842d"

@interface DataSource()

@property(nonatomic, strong, readonly) AFHTTPSessionManager *sessionManager;

@end

@implementation DataSource

+ (DataSource*)sharedInstance {
    
    static DataSource *sharedInstance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DataSource alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init{
    
    AFHTTPSessionManager *sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://gateway.marvel.com/"]];
    
    sessionManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    return [self initWithSessionManager:sessionManager];
}

- (instancetype)initWithSessionManager:(AFHTTPSessionManager *)sessionManager {
    self = [super init];
    
    if (self) {
        _sessionManager = sessionManager;
    }
    
    return self;
}

- (void)loadCharacters:(Page *)page complete:(void(^)(NSArray<Character *> *))complete error:(void(^)(void))error{
        
    NSUInteger timestamp = [[NSDate new] timeIntervalSince1970];
    NSString *hash = [NSString stringWithFormat:@"%lu%@%@", (unsigned long)timestamp, PRIVATE_API_KEY, PUBLIC_API_KEY];
    
    NSDictionary *parameters = @{
                                 @"limit": @(page.limit),
                                 @"offset": @(page.offset),
                                 @"apikey": PUBLIC_API_KEY,
                                 @"ts": @(timestamp),
                                 @"hash": [hash md5]
                                 };
    
    [self.sessionManager GET:@"v1/public/characters" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
        NSDictionary *data = [responseObject valueForKey:@"data"];
        NSArray *results = [data valueForKey:@"results"];
        NSMutableArray<Character *> *characters = [NSMutableArray arrayWithCapacity:results.count];
        
        for (NSDictionary *item in results) {
            
            Character *character = [self setCharacter:item];
    
            [characters addObject:character];
        }
        
        if (complete) {
            complete(characters);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull err) {
        if (error) {
            error();
        }
    }];
}

- (void)loadCharacterComics:(NSString *)characterId complete:(void (^)(NSArray<Comics *> *))complete error:(void (^)(void))error {

    NSString *url = [NSString stringWithFormat:@"v1/public/characters/%@/comics", characterId];
    
    NSUInteger timestamp = [[NSDate new] timeIntervalSince1970];
    NSString *hash = [NSString stringWithFormat:@"%lu%@%@", (unsigned long)timestamp, PRIVATE_API_KEY, PUBLIC_API_KEY];
    
    NSDictionary *parameters = @{
                                 @"limit": @(10),
                                 @"offset": @(0),
                                 @"orderBy": @"-focDate",
                                 //@"startYear":@(2005),
                                 @"apikey": PUBLIC_API_KEY,
                                 @"ts": @(timestamp),
                                 @"hash": [hash md5]
                                 };
    
    [self.sessionManager GET: url parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject) {

        NSDictionary *data = [responseObject valueForKey:@"data"];
        NSArray *results = [data valueForKey:@"results"];
        NSMutableArray<Comics *> *comicsArr = [NSMutableArray arrayWithCapacity:results.count];
        
        for (NSDictionary *item in results) {
                
            Comics *comics = [self setComics:item];
                
            [comicsArr addObject:comics];
        }
        
        if (complete) {
            complete(comicsArr);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull err) {
        if (error) {
            error();
        }
    }];
}

- (Character *)setCharacter:(NSDictionary *)item {
    Character *character = [Character new];
    character.characterId = [item valueForKey:@"id"];
    character.name = [item valueForKey:@"name"];
    character.characterDescription = [item valueForKey:@"description"];
    
    NSDictionary *thumbnail = [item valueForKey:@"thumbnail"];
    NSString *thumbnailUrl = [NSString stringWithFormat:@"%@.%@", [thumbnail valueForKey:@"path"], [thumbnail valueForKey:@"extension"]];
    character.thumbnailUrl = [thumbnailUrl stringByReplacingOccurrencesOfString:@"http" withString:@"https"];
    
    return character;
}

- (Comics *)setComics:(NSDictionary *)item{
    Comics *comics = [Comics new];
    comics.comicsId = [item valueForKey:@"id"];
    comics.title = [item valueForKey:@"title"];
    
    NSDictionary *thumbnail = [item valueForKey:@"thumbnail"];
    NSString *thumbnailUrl = [NSString stringWithFormat:@"%@.%@", [thumbnail valueForKey:@"path"], [thumbnail valueForKey:@"extension"]];
    comics.thumbnailUrl = [thumbnailUrl stringByReplacingOccurrencesOfString:@"http" withString:@"https"];

    return comics;
}

@end

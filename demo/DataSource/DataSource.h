//
//  DataSource.h
//  demo
//
//  Created by Çağatay Özcan on 2.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class Character;
@class Page;
@class Comics;

@interface DataSource : NSObject

+ (DataSource*)sharedInstance;

- (void)loadCharacters:(Page *)page complete:(void(^)(NSArray<Character *> *))complete error:(void(^)(void))error;
- (void)loadCharacterComics:(NSString *)characterId complete:(void (^)(NSArray<Comics *> *))complete error:(void (^)(void))error;

@end

NS_ASSUME_NONNULL_END

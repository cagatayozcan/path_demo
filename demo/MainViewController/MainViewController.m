//
//  MainViewController.m
//  demo
//
//  Created by Çağatay Özcan on 2.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import "MainViewController.h"
#import "GetCharactersData.h"
#import "CharacterCell.h"
#import "UIImageView+AFNetworking.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+WebCache.h"
#import "DetailViewController.h"

@interface MainViewController ()

@property(nonatomic, strong) UIActivityIndicatorView *indicator;
@property(nonatomic, strong) UIActivityIndicatorView *loadMoreIndicator;

@end

@implementation MainViewController

@synthesize tableView;

- (void)viewDidLoad {
        
    self.objects = [NSMutableArray array];
    
    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.loadMoreIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.tableView.backgroundView = self.indicator;
    self.tableView.tableFooterView = self.loadMoreIndicator;
    
    [[[GetCharactersData sharedInstance] initWithView:self] loadCharacters];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)showCharacters {
    [self.tableView reloadData];
}

- (void)setLoadingIndicator:(BOOL)active {
    if (active) {
        [self.indicator startAnimating];
    } else {
        [self.indicator stopAnimating];
    }
}

- (void)setMoreLoadingIndicator:(BOOL)active {
    if (active) {
        [self.loadMoreIndicator startAnimating];
    } else {
        [self.loadMoreIndicator stopAnimating];
    }
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CharacterCell";
    
    CharacterCell *cell = (CharacterCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CharacterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    Character *object = self.objects[indexPath.row];
    
    cell.nameLb.text = object.name;
    
    [cell.thumbnailImgView sd_setImageWithURL:[NSURL URLWithString: object.thumbnailUrl]];
    
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL lastItemReached = indexPath.row == self.objects.count - 1;
    
    if (lastItemReached) {
        [[GetCharactersData sharedInstance] loadMoreCharacters];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Character *object = self.objects[indexPath.row];
    
    DetailViewController *controller = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
    controller.character = object;
    [self.navigationController pushViewController:controller animated:YES];
}


@end

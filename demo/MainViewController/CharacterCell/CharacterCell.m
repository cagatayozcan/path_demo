//
//  CharacterCell.m
//  demo
//
//  Created by Çağatay Özcan on 5.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import "CharacterCell.h"

@implementation CharacterCell

@synthesize nameLb;
@synthesize thumbnailImgView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        NSArray *a = [[NSBundle mainBundle] loadNibNamed:@"CharacterCell" owner:self options:nil];
        if ( a )    {
            self = [a objectAtIndex:0];
        }
        else    {
            NSLog( @"Error loading CharacterCell.xib");
            self = nil;
        }
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
            
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

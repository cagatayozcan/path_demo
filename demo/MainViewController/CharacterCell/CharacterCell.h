//
//  CharacterCell.h
//  demo
//
//  Created by Çağatay Özcan on 5.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIView+WebCache.h>

NS_ASSUME_NONNULL_BEGIN

@interface CharacterCell : UITableViewCell{
    UILabel *nameLb;
    UIImageView *thumbnailImgView;
}

@property (strong, nonatomic) IBOutlet UILabel *nameLb;
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailImgView;

@end

NS_ASSUME_NONNULL_END

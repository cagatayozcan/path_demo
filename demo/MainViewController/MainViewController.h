//
//  MainViewController.h
//  demo
//
//  Created by Çağatay Özcan on 2.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Character.h"

@interface MainViewController : UIViewController {
    UITableView *tableView;
}

@property(atomic, strong) NSMutableArray<Character *> *objects;
@property(nonatomic, strong) IBOutlet UITableView *tableView;

- (void)showCharacters;
- (void)setLoadingIndicator:(BOOL)active;
- (void)setMoreLoadingIndicator:(BOOL)active;

@end

//
//  GetCracterData.h
//  demo
//
//  Created by Çağatay Özcan on 2.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GetCharactersData : NSObject

+ (GetCharactersData*)sharedInstance;
- (instancetype)init;
- (instancetype)initWithView:(MainViewController *)view;
- (void)loadCharacters;
- (void)loadMoreCharacters;

@end

NS_ASSUME_NONNULL_END

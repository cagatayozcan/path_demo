//
//  GetCracterData.m
//  demo
//
//  Created by Çağatay Özcan on 2.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import "GetCharactersData.h"
#import "Page.h"
#import "DataSource.h"

static const NSUInteger DEFAULT_LIMIT = 30;

@interface GetCharactersData()

@property(nonatomic, strong) Page *currentPage;
@property(nonatomic, weak) MainViewController *view;

@end

@implementation GetCharactersData

+ (GetCharactersData*)sharedInstance {
    
    static GetCharactersData *sharedInstance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[GetCharactersData alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init{
    return [self initWithView:self.view];
}

- (instancetype)initWithView:(MainViewController *)view {
    self = [super init];
    
    if (self) {
        self.currentPage = [[Page alloc] initWithLimit:DEFAULT_LIMIT andOffset:0];
        self.view = view;
    }
    
    return self;
}

- (void)loadCharacters {
    
    [self.view setLoadingIndicator:YES];
    self.currentPage = [[Page alloc] initWithLimit:DEFAULT_LIMIT andOffset:0];
        
    [[DataSource sharedInstance] loadCharacters:self.currentPage complete:^(NSArray<Character *> *characters) {
        [self.view.objects addObjectsFromArray:characters];
        [self.view showCharacters];
        [self.view setLoadingIndicator:NO];
    } error:nil];
}

- (void)loadMoreCharacters {
    
    [self.view setMoreLoadingIndicator:YES];
    self.currentPage.offset += DEFAULT_LIMIT;
        
    [[DataSource sharedInstance] loadCharacters:self.currentPage complete:^(NSArray<Character *> *characters) {
        [self.view.objects addObjectsFromArray:characters];
        [self.view showCharacters];
         [self.view setMoreLoadingIndicator:NO];
        
    } error:nil];
}

@end

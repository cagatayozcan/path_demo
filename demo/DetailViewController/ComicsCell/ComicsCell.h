//
//  ComicsCell.h
//  demo
//
//  Created by Çağatay Özcan on 7.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ComicsCell : UITableViewCell{
    UILabel *titleLb;
    UIImageView *thumbnailImgView;
}

@property (strong, nonatomic) IBOutlet UILabel *titleLb;
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailImgView;

@end

NS_ASSUME_NONNULL_END

//
//  DetailViewController.m
//  demo
//
//  Created by Çağatay Özcan on 2.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import "DetailViewController.h"
#import "GetCharacterDetailData.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ComicsCell.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

@synthesize character;
@synthesize nameLb;
@synthesize thumbnailImgView;
@synthesize descriptionTv;
@synthesize tableView;
@synthesize objects;

- (void)viewDidLoad {
    
    self.objects = [NSMutableArray array];
    
    [[[GetCharacterDetailData sharedInstance] initWithView:self] loadCharacterComics:[NSString stringWithFormat:@"%@",self.character.characterId]];
    
    [self.thumbnailImgView sd_setImageWithURL:[NSURL URLWithString:self.character.thumbnailUrl]];
    self.nameLb.text = self.character.name;
    self.descriptionTv.text = self.character.characterDescription;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)showComics {
    [self.tableView reloadData];
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CharactComicsCellerCell";
    
    ComicsCell *cell = (ComicsCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ComicsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    Comics *object = self.objects[indexPath.row];
    
    cell.titleLb.text = object.title;
    
    [cell.thumbnailImgView sd_setImageWithURL:[NSURL URLWithString: object.thumbnailUrl]];
        
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return NO;
}

@end

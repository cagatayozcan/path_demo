//
//  GetCharacterDetailData.h
//  demo
//
//  Created by Çağatay Özcan on 7.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DetailViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GetCharacterDetailData : NSObject

+ (GetCharacterDetailData*)sharedInstance;
- (instancetype)init;
- (instancetype)initWithView:(DetailViewController *)view;
- (void)loadCharacterComics:(NSString *)characterId;

@end

NS_ASSUME_NONNULL_END

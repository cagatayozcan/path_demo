//
//  GetCharacterDetailData.m
//  demo
//
//  Created by Çağatay Özcan on 7.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import "GetCharacterDetailData.h"
#import "DataSource.h"

@interface GetCharacterDetailData()

@property(nonatomic, weak) DetailViewController *view;

@end

@implementation GetCharacterDetailData

+ (GetCharacterDetailData*)sharedInstance {
    
    static GetCharacterDetailData *sharedInstance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[GetCharacterDetailData alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init{
    return [self initWithView:self.view];
}

- (instancetype)initWithView:(DetailViewController *)view {
    self = [super init];
    
    if (self) {
        self.view = view;
    }
    
    return self;
}

- (void)loadCharacterComics:(NSString *)characterId {
    
    [[DataSource sharedInstance] loadCharacterComics:characterId complete:^(NSArray<Comics *> *comics) {
        
        [self.view.objects addObjectsFromArray:comics];
        [self.view showComics];
        
    } error:nil];
}



@end

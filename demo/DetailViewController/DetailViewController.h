//
//  DetailViewController.h
//  demo
//
//  Created by Çağatay Özcan on 2.11.2021.
//  Copyright © 2021 Ozcan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Character.h"
#import "Comics.h"

NS_ASSUME_NONNULL_BEGIN

@interface DetailViewController : UIViewController{
    
    Character *character;
    UILabel *nameLb;
    UITextView *descriptionTv;
    UIImageView *thumbnailImgView;
}
@property(nonatomic, strong) Character *character;
@property(nonatomic, strong) IBOutlet UILabel *nameLb;
@property(nonatomic, strong) IBOutlet UIImageView *thumbnailImgView;
@property(nonatomic, strong) IBOutlet UITextView *descriptionTv;

@property(atomic, strong) NSMutableArray<Comics *> *objects;
@property(nonatomic, strong) IBOutlet UITableView *tableView;

- (void)showComics;

@end

NS_ASSUME_NONNULL_END
